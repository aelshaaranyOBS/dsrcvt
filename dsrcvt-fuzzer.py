#! /usr/bin/env python

# ****************************************************************************
# Author(s):
#   Ryan Ewing (January 2018)
# Description:
#   A simple command-line fuzzer that tries to send a BSM for all possible values of the
#   selected parameters. It currently only tests the full ranges of values for
#   each parameter, but does not test values outside of that accepted range. Has the
#   ability to send a certain number of packets for each value at a desired rate.
#
# Copyright (C) 2017 - 2018 OnBoard Security, Inc.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ****************************************************************************

from utils.wsmp import WSMP
import argparse
import time

# Define argument parser
parser = argparse.ArgumentParser(description="Transmit [Un]Signed DSRC messages",
                                 version="1.0.1")

# Define positional arguments
parser.add_argument("period", type=float, default=0,
                    help="rate of transmitting messages in seconds (0 for warp speed)")
parser.add_argument("num_packets", type=int, default=1,
                    help="number of packets to transmit for each value.")

# Define optional arguments
parser.add_argument("-S", "--signed", action="store_true",
                    help="use to send signed messages, unsigned messages are sent otherwise")
parser.add_argument("-V", "--verbose", action="store_true",
                   help="show verbose logs as the program executes")
parser.add_argument("-i", "--udpip", default="localhost",
                    help="IP address of the host")
parser.add_argument("-p", "--udpport", default=52001, type=int,
                    help="port number in gnuradio_companion")
parser.add_argument("-w", "--whitelist", nargs='+', choices=['all', 'bsm_id', 'bsm_type', 'msg_cnt', 'dsrc_id', 'time_stamp', 'latitude', 'longitude', 'elevation', 'semimajor_acc',
                        'semiminor_acc', 'semimajor_orient', 'transmission_state', 'speed', 'heading', 'steer_angle', 'accel_lon', 'accel_lat', 'accel_vert', 'yaw_rate', 
                        'break_applied', 'traction_status', 'abs_status', 'stability_status', 'brake_boost_status', 'park_brake_status', 'v_width', 'v_length'],
                        metavar='field', help="Whitelist the fields to fuzz.")

# Parsing all arguments
args = parser.parse_args()

# Create a custom range() function that supports incrementing by floats
def myRange(lower, upper, increment):
    num = lower
    while num < upper + increment:
        yield num
        num += increment

# The acceptable input range for each parameter.
# The fuzzer tries all values within the range.
# TODO: add values outside range to test effects of unexpected inputs
parameters =      {
                    'bsm_id' : [0x14, 0x14],
                    'bsm_type' : [0x0, 0x0],
                    'msg_cnt' : [0, 127],
                    'dsrc_id' : [0x00000000, 0xFFFFFFFF],
                    'time_stamp' : [0, 99999],
                    'latitude' : [-90, 90],
                    'longitude' : [-180, 180],
                    'elevation' : [0, 65535],
                    'semimajor_acc' : [0, 255],
                    'semiminor_acc' : [0, 255],
                    'semimajor_orient' : [0, 360],
                    'transmission_state' : [0, 7],
                    'speed' : [0, 163.82],
                    'heading' : [0, 360],
                    'steer_angle' : [-189, 189],
                    'accel_lon' : [-20, 20.01],
                    'accel_lat' : [-20, 20.01],
                    'accel_vert' : [-2.52, 2.54],
                    'yaw_rate' : [-32767, 32767],
                    'break_applied' : [0b00000, 0b11111],
                    'traction_status' : [0, 3],
                    'abs_status' : [0, 3],
                    'stability_status' : [0, 3],
                    'brake_boost_status' : [0, 3],
                    'park_brake_status' : [0, 3],
                    'v_width' : [0, 1023],
                    'v_length' : [0, 4095],
                }


bsm = WSMP(args.verbose)

if 'all' in args.whitelist:
    whitelist = parameters.keys()
else:
    whitelist = args.whitelist

for p in whitelist:
    print "Fuzzing", p
    # For parameters to be incremented by 0.02
    if p == 'speed' or p == 'accel_vert':
        for n in myRange(parameters[p][0], parameters[p][1], 0.02):
            bsm.edit(p, n)
            msg = bsm.construct(args.signed)
            for i in xrange(args.num_packets):
                bsm.send(msg, args.udpip, args.udpport)
                time.sleep(args.period)
    # For parameters to be incremented by 0.01
    elif p == 'accel_lon' or p == 'accel_lat':
        for n in myRange(parameters[p][0], parameters[p][1], 0.01):
            bsm.edit(p, n)
            msg = bsm.construct(args.signed)
            for i in xrange(args.num_packets):
                bsm.send(msg, args.udpip, args.udpport)
                time.sleep(args.period)
    # For parameters to be incremented by 1 (all others)
    else:
        for n in myRange(parameters[p][0], parameters[p][1], 1):
            bsm.edit(p, n)
            msg = bsm.construct(args.signed)
            for i in xrange(args.num_packets):
                bsm.send(msg, args.udpip, args.udpport)
                time.sleep(args.period)
