#! /usr/bin/env python

# ****************************************************************************
# Author(s):
#   Ryan Ewing (January 2018)
# Description:
#   A command-line program to create and send BSMs using the WSMP class defined
#   in utils/wsmp.py. Supports sending signed and unsigned BSMs, with the option
#   to edit values of parameters using the --edit FIELD VALUE or -e FIELD VALUE
#   argument.
#
# Copyright (C) 2017 - 2018 OnBoard Security, Inc.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ****************************************************************************

from utils.wsmp import WSMP
from datetime import datetime

import argparse
import time

# Define argument parser
parser = argparse.ArgumentParser(description="Transmit [Un]Signed DSRC messages",
                                 version="1.0.1")

# Define optional arguments
parser.add_argument("-S", "--signed", action="store_true",
                    help="use to send signed messages, unsigned messages are sent otherwise")
parser.add_argument("-V", "--verbose", action="store_true",
                   help="show verbose logs as the program executes")
parser.add_argument("-i", "--udpip", default="localhost",
                    help="IP address of the host")
parser.add_argument("-p", "--udpport", default=52001, type=int,
                    help="port number in gnuradio_companion")
parser.add_argument("-e", "--edit", nargs=2, action='append', metavar=('field', 'value'), 
                    help="set a BSM field to a specific value. Fields: {'bsm_id', 'bsm_type', 'msg_cnt', 'dsrc_id', 'time_stamp', 'latitude', 'longitude', 'elevation', 'semimajor_acc', 'semiminor_acc', 'semimajor_orient', 'transmission_state', 'speed', 'heading', 'steer_angle', 'accel_lon', 'accel_lat', 'accel_vert', 'yaw_rate', 'break_applied', 'traction_status', 'abs_status', 'stability_status', 'brake_boost_status', 'park_brake_status', 'v_width', 'v_length'}")

# Define positional arguments
parser.add_argument("msgtype", choices=['bsm', 'spat', 'map'],
                    help="type of message to be transmitted")
parser.add_argument("period", type=float, default=0,
                    help="rate of transmitting messages in seconds (0 for warp speed)")
parser.add_argument("num_packets", type=int, default=-1,
                    help="number of packets to transmit (-1 for infinite). It is recommended to send at least 2 as there may be a buffer delay of one packet.")

# Parsing all arguments
args = parser.parse_args()

bsm = WSMP(args.verbose)

# edit the values the user specified
if args.edit: # equals false if args.edit is empty
    for param in args.edit:
        bsm.edit(param[0], param[1])
        print "Set %s to %s" %(param[0], param[1])

wsm = bsm.construct(signed=args.signed)

# currently only supports BSM
# TODO: implement SPAT and MAP messages
if (args.msgtype == 'bsm'):
    count = 0
    while (count < args.num_packets or args.num_packets == -1):
        count += 1
        bsm.send(wsm, args.udpip, args.udpport)
        print "BSM #%s sent. (%s)" %(count, ('Signed' if args.signed else 'Unsigned'))
        time.sleep(args.period)
    print "Sent %s BSM packets." %(args.num_packets)
