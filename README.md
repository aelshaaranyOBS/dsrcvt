# About

The DSRC Validation Tool (DSRCVT) is a tool to facilitate testing attacks on
automotive onboard units. There are several versions of the program.

The main programs are `dsrcvt.py`, `dsrcvt-crafter.py`, and `dsrcvt-crafter-gui.py`.

The `dsrcvt.py` program sends unsigned or signed Basic Safety Messages by
re-signing a recorded BSM sent for automotive onboard units.

The `dsrcvt-crafter.py` is an improvement on the `dsrcvt.py` program, as it
crafts entirely custom BSMs from scratch, with the proper Wave Short Message
Protocol (WSMP) layers on top using a WSMP library found in `utils/wsmp.py`.

The `dsrcvt-crafter-gui.py` is a GUI version of the `dsrcvt-crafter.py`, which
is a command line tool.

There are a few additional files that may be useful. The `xml_to_uper.py` program
contains a function which creates an unsigned UPER encoded BSM by utilizing the
J2735 ASN file compiled into C code. A C binary which converts XML formatted
BSMs to UPER encoded ones is included named `bsm_uper`.


If one wants to create a new tool that sends BSMs, the WSMP class from `wsmp.py`
may be useful, as it contains all the functions needed to edit, construct, and
send a BSM or a similar Wave Short Message (WSM).

# Usage

#### DSRCVT Crafter Command-Line Tool:
```sh
dsrcvt-crafter.py [-h] [-v] [-S] [-V] [-i UDPIP] [-p UDPPORT]
                  [-e field value]
                  {bsm,spat,map} period num_packets
```

#### DSRCVT Fuzzer Tool:
```sh
dsrcvt-fuzzer.py [-h] [-v] [-S] [-V] [-i UDPIP] [-p UDPPORT]
                 [-w field [field ...]]
                 period num_packets
```

#### DSRCVT Tool:
```sh
dsrcvt.py [-h] [-v] [-s] [-V] [-i UDPIP] [-p UDPPORT]
          {bsm,spat,map} period num_packets
```

#### xml\_to\_uper.py:
import into your python program with `from xml_to_uper import createXML_BSM`
and ensure the `bsm_uper` binary is included in the same directory.

#### bsm\_uper:
    `./bsm_uper XML_IN_FILE UPER_OUT_FILE`

# CONTACT
For any help with the tool, contact Raashid Ansari at mransari@onboardsecurity.com.

