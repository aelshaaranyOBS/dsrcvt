#! /usr/bin/env python

# ****************************************************************************
# Author(s):
#   Ahmed Elsharaany (September 2017)
#   Raashid Ansari (November 2017)
#   Ryan Ewing (January 2018)
#
# Copyright (C) 2017 -  OnBoard Security, Inc.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ****************************************************************************

'''
Description:
    A class to create and send Wave Short Messages conforming to the Wave
    Short Message Protocol. Specifically, the messages being created are
    SAE J2735 Basic Safety Messages (BSMs). Supports sending both signed and
    unsigned BSMs with the ability to edit parameters of the BSM using the edit()
    function.

    The useful functions are
        edit(parameter, new_value) # sets the specified parameter to the new value
        construct(signed=False) # constructs and returns a WSMP packet with a signed or unsigned BSM
        send(wsm, udp_ip, udp_port) # sends a constructed WSMP packet to the ip and port specified

'''

import socket
import struct
import collections
import time
import binascii


from subprocess import call

from datetime import datetime
from bitarray import bitarray

import xml.etree.ElementTree as et
from xml.dom import minidom

import hashlib
from cryptography.hazmat.primitives.asymmetric import ec, utils
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes

class WSMP(object):
    def __init__(self, verbose=False):
        # All parameters needed for BSM
        # Each dictionary entry is structured as follows:
        #     'NAME': [VALUE, 'BIT LENGTH', [MULTIPLIER, SHIFT], "DESCRIPTION"]
        #     VALUE: The value of the parameter
        #     BIT LENGTH: The number of bits for that parameter in the form of
        #                 '0[num bits]b', look up the 'format' function for
        #                 more information.
        #     [MULTIPLIER, SHIFT]: Multiplier and shift are used to convert
        #                          from human readable units (m/s, degrees,
        #                          etc) to the integer number format the packet
        #                          uses, e.g. 90 degrees latitude is converted
        #                          to the integer 1800000000 for the packet.
        #     DESCRIPTION: A short description of the parameter and its allowed
        #                          values.
        # TODO: Move parameter names and values into seperate dictionary for easier use
        #        WARNING: Moving names and values will break dsrcat-crafter-gui.py.
        self.parameters =   collections.OrderedDict([
                                ('bsm_id',             [0x0014,        '016b', [1, 0],                 "BSM ID, should always be 20 / 0x14."]),
                                ('bsm_type',           [0b00000000000, '011b', [1, 0],                 "BSM Type."]),
                                ('msg_cnt',            [1,             '07b',  [1, 0],                 "Message count."]),
                                ('dsrc_id',            [0x51f50838,    '032b', [1, 0],                 "DSRC ID, 4 byte unit id."]),
                                ('time_stamp',         [-1,            '016b', [1, 0],                 "The timestamp to use when sending the packet. Set to -1 to use the time when sending."]),
                                ('latitude',           [90,            '031b', [10000000, 900000000],  "Current vehicle latitude, enter from -90.00 to 90.00 degrees."]),
                                ('longitude',          [180,           '032b', [10000000, 1800000000], "Current vehicle longitude, enter from -180.00 to 180.00 degrees."]),
                                ('elevation',          [0,             '016b', [1, 4096],              "Current vehicle elevation, enter from -4096 to 61439 cm."]),
                                ('semimajor_acc',      [255,           '08b',  [1, 0],                 "Semi major axis accuracy, used for determining vehicle position."]),
                                ('semiminor_acc',      [255,           '08b',  [1, 0],                 "Semi minor axis accuracy, used for determining vehicle position."]),
                                ('semimajor_orient',   [360,           '016b', [65535.0 / 360.0, 0],   "Semi major axis orientation, used for determining vehicle position."]),
                                ('transmission_state', [7,             '03b',  [1, 0],                 "Transmission state (current gear)-  0 neutral, 1 park, 2 forward, 3 reverse, 4 reserved1, 5 reserved2, 6 reserved3, 7 unavailable."]),
                                ('speed',              [163.82,        '013b', [50, 0],                "Vehicle speed, from 0 to 163.8 m/s. A value of 163.82 means speed unavailable."]),
                                ('heading',            [360,           '015b', [80, 0],                "Direction vehicle is moving, enter a degree value greater or equal to 0 and less than 360. A value of 360 means heading unavailable."]),
                                ('steer_angle',        [0,             '08b',  [1/1.5, 126],           "Driver's steering wheel angle from -189 to 189 degrees."]),
                                ('accel_lon',          [0,             '012b', [100, 2000],            "Driver's acceleration in the longitudinal direction in units of m/s^2."]),
                                ('accel_lat',          [0,             '012b', [100, 2000],            "Driver's acceleration in the latitudinal direction in units of m/s^2."]),
                                ('accel_vert',         [0,             '08b',  [50, 127],              "Driver's acceleration along the verical of the vehicle in units of 0.02g."]),
                                ('yaw_rate',           [0,             '016b', [1, 32767],             "Yaw rate of vehicle, in units of 0.01 degrees per second (signed)."]),
                                ('break_applied',      [0b10000,       '05b',  [1, 0],                 "5 bit string determining which breaks are currently engaged. 1st bit on means data unavailable, 2nd bit is for Left Front, 3rd for Left Rear, 4th for Right Front, 5th for Right Rear."]),
                                ('traction_status',    [0,             '02b',  [1, 0],                 "Provides information about tractions control status. A value of 0 means unavailable or not equipped, 1 means traction control off, 2 means traction control on but not engaged, 3 means traction control on and engaged."]),
                                ('abs_status',         [0,             '02b',  [1, 0],                 "Provides information about Anti Lock Brake status. 0 means unavailable or not equipped, 1 means off, 2 means on, 3 means on and engaged."]),
                                ('stability_status',   [0,             '02b',  [1, 0],                 "Provides information about Stability Control status. 0 means unavailable or not equipped, 1 means off, 2 means on, 3 means on and engaged."]),
                                ('brake_boost_status', [0,             '02b',  [1, 0],                 "Provides information about Brake Boost status. 0 means unavailable or not equipped, 1 means off, 2 means on."]),
                                ('park_brake_status',  [0,             '02b',  [1, 0],                 "Provides information about Parking Brake status. 0 means unavailable or not equipped, 1 means off, 2 means on, 3 is reserved."]),
                                ('v_width',            [0,             '010b', [1, 0],                 "Width of the vehicle in units of 1 cm. Max value is 10 m (1000 cm)."]),
                                ('v_length',           [0,             '012b', [1, 0],                 "Length of vehicle in units of 1 cm. Max value is 40 m (4000 cm)."]),
                            ])
        self.verbose = verbose

    def getParameters(self):
        return self.parameters

    # Construct packet
    def construct(self, signed=False):
        self.printMsg("WSMP: Creating %s Basic Safety Message." %("signed" if signed else "unsigned"))
        self.printMsg('\n')
        self.printMsg("Preparing BSM message...")
        bsm = self.constructBSM(signed)

        wsm = ""

        self.printMsg('\n')
        self.printMsg("Constructing packet...")
        wsm += self.constructLLC()
        wsm += self.constructNHeader()
        wsm += self.constructTHeader(len(bsm))
        self.printMsg("--- Appending BSM ---")
        wsm += bsm

        self.printMsg('\n')
        self.printMsg("Packet complete!")

        return wsm

    # Append Logical Link Control, do not change values unless you want to send a non WSM message
    def constructLLC(self):
        self.printMsg("--- Appending LLC ---")
        dsap = 0xaa
        ssap = 0xaa
        ctrl_field = 0x03 # unumbered frame
        msg_type = 0x88dc # WSM
        return struct.pack("!6BH", dsap, ssap, ctrl_field, 0x00, 0x00, 0x00, msg_type)

    # Append WSMP N Header to packet
    def constructNHeader(self):
        self.printMsg("--- Appending WSMP N Header ---")
        sub = 0x0000
        ver = 0x3
        opt = 0x0
        return struct.pack("!2B", sub << 4 | (opt << 3) | ver, 0x00)

    # Append WSMP T Header to packet
    def constructTHeader(self, msg_len):
        self.printMsg("--- Appending WSMP T Header ---")
        psid = 0x00
        return struct.pack("!BH", psid, 0x8000 | msg_len)


    # Add a parameter to BSM message
    def addParameter(self, param):
        p_bytes = bitarray()
        # if parameter is time_stamp, use current time if equal to -1
        if param == 'time_stamp' and self.parameters[param][0] == -1:
            t = datetime.now()
            ts = t.second*1000 + t.microsecond/1000
            self.printMsg("Adding %s with value %s" %(param, ts))
            p_bytes.extend(format(ts, self.parameters[param][1]))

        else:
            self.printMsg("Adding %s with value %s" %(param, self.parameters[param][0]))
            p_bytes.extend(format(int(self.parameters[param][0] * self.parameters[param][2][0] + self.parameters[param][2][1]), self.parameters[param][1]))
        return p_bytes

    # Construct BSM message
    def constructBSM(self, signed=False):
        self.printMsg("--- Constructing BSM ---")
        bsm = bitarray()
        for param in self.parameters:
            bsm += self.addParameter(param)

        if signed:
            return self.signBSM(bsm)

        return bsm.tobytes()


    def getTime64(self):

        # subtract the difference between 1970 and 2004 from current time
        TIME_DIFF_CONST = 1072915200
        time_now_sub = time.time() - TIME_DIFF_CONST

        # multiply by 1M and convert to int
        time_now_64 = int(time_now_sub * 1000000)

        return time_now_64

    ## Define craft BSM with valid signature function
    def craftBSMValidSignature(self, tbs_data, cert_hash_bytes, sign_key_bytes):
        # Certificate bytes grabbed from the enrollment_certificate.dat (this
        # file has to be obtained by the user themselves). Since certificate
        # doesn't change and we only want part of the .dat file, it is more
        # efficient to hard code the bytes than read from the file every time.
        # The bytes you want here are the ones that correspond to the
        # bytes before time, time bytes and bytes after time before signature
        # part of the certificate.
        CERTIFICATE_BYTES = bytearray.fromhex('')

        # hash the to be signed data 
        tbs_hash = hashlib.sha256(tbs_data)

        # concatenate the two hashes together and prepare it for signing
        concatenated_hash = tbs_hash.hexdigest() + binascii.hexlify(''.join(cert_hash_bytes))

        # generate signing key from signing key bytes, and specifiy the eliptical curve
        # sk = SigningKey.from_string(bytearray(sign_key_bytes), curve=NIST256p)
        sk = ec.derive_private_key(int(str(bytearray(sign_key_bytes)).encode('hex'), 16), ec.SECP256R1(), default_backend())

        # set r and s lengths to 0 (sometimes signing generates r and s greater or less than 64)
        r_len = 0
        s_len = 0

        # loop until valid signature length is achived
        while( r_len != 64 or s_len != 64 ):
            # sign the concatenated hash (note that the signing function does hashing internally using the specificed hashing algorithm
            # so there is no need to do hashing before signing)
            # signature = sk.sign(bytearray.fromhex(concatenated_hash), hashfunc=hashlib.sha256)
            signature = sk.sign(str(bytearray.fromhex(concatenated_hash)), ec.ECDSA(hashes.SHA256()))

            # generate r and s from the signautre
            # [r,s] = util.sigdecode_string(signature, NIST256p.order)
            (r,s) = utils.decode_dss_signature(signature)

            # convert r and s to hex strings
            r_hex = hex(r)
            s_hex = hex(s)
            r_len = len(r_hex[2:len(r_hex)-1])
            s_len = len(s_hex[2:len(s_hex)-1])


        # Concatenate the r ans s signature bytes
        signed_message = bytearray.fromhex(r_hex[2:len(r_hex)-1] + s_hex[2:len(s_hex)-1])


        ''' For testing using Aerolink
        f_out = open("Attool_Signed_Message.dat","wb")
        f_out.write(bytearray.fromhex((signed_message)))
        f_out.close()
        '''

        # Return the concatenated certificate and signed message
        return CERTIFICATE_BYTES + signed_message

    def signBSM(self, bsm):
        sig = bitarray()
        sig.extend(format(0x03, '08b')) # Protocol version (0x03)
        sig.extend(format(0x81, '08b')) # ContentType signedData (0x81)
        sig.extend(format(0x00, '08b')) # Hash algorithm - SHA256 (0x00)

        sig.extend(format(0x40, '08b'))

        # to be signed data
        tbs = bitarray()
        tbs.extend(format(0x03, '08b')) # Protocol version (0x03)
        tbs.extend(format(0x80, '08b')) # ContentType unsecuredData (0x80)
        tbs.extend(format(len(bsm.tobytes()), '08b')) # Length of unsigned BSM message

        tbs += bsm # Attach unsigned BSM message

        tbs.extend(format(0x40, '011b'))
        tbs.extend(format(0x01, '08b'))
        tbs.extend(format(0x20, '08b'))
        tbs.extend(format(self.getTime64(), '064b')) # Add a 64 bit timestamp as specified in the IEE 1609.2 standard

        with open("utils/Cert_Hash.dat", 'rb') as f:
            cert = f.read()

        with open("utils/Sign_Key.dat", 'rb') as f:
            sign_key = f.read()

        return sig.tobytes() + tbs.tobytes() + self.craftBSMValidSignature(tbs, cert, sign_key)

    # Send packet
    def send(self, wsm, udp_ip, udp_port):
        self.printMsg('\n')
        self.printMsg("Sending packet...")
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.sendto(wsm, (udp_ip, int(udp_port)))
        sock.close()
        self.printMsg("Packet sent!")
        self.printMsg('\n')

    # Edit BSM parameter
    def edit(self, param, val):
        if param in self.parameters:
            prev_val = self.parameters[param][0]
            self.parameters[param][0] = float(val)
            self.printMsg("Changed %s from %s to %s" %(param, prev_val, val))

        else:
            print "ERROR: Unable to change %s to %s. No such parameter." %(param, val)

    # Custom print function to identify prints from the WSMP class
    def printMsg(self, msg):
        if self.verbose:
            if msg == '\n':
                print
            else:
                print "WSMP: %s - %s" %(datetime.now().strftime('%H:%M:%S.%f'),msg)
