## This repo is covered by different licensing depending on one's use case

# DSRC Validation Tool Licensing

OnBoard Security DSRC Validation Tool (dsrcvt/DSRCVT) software invention and reference source
code including `dsrcvt`, `dsrcvt-crafter`, `dsrcvt-fuzzer` is a **dual license**
product available under open source and standard commercial licensing. 

# GPL
DSRCVT may be used as long as the user adheres to version two (2)
or higher of the GPL License. For details please refer to COPYING-2.txt
included in this distribution. 

The GPLv2 license may also be found on the gnu.org website at:
(http://www.gnu.org/licenses/gpl-2.0.html)

# FOSS Exception
DSRCVT IP used under the GPL may be included in other Free and Open Source
Software (FOSS) products subject to the conditions in [FOSS Exception.md][1]

The grant of the ability to use DSRCVT under the GPL in other FOSS projects is
an irrevocable grant by OnBoard Security, Inc. and cannot be withdrawn by
OnBoard Security, Inc, and/or any future owners of DSRCVT.

# Commercial Licensing
Businesses and enterprises that wish to incorporate DSRCVT into proprietary
appliances or other commercial software products for re-distribution must have
a commercial license. Commercial licenses are available using flexible
licensing terms on a one time per product fee or running royalty. For details
please contact sales [at] onboardsecurity [dot] com

[1]: https://bitbucket.org/onboardsecurity/dsrcvt/src/master/FOSS%20Exception.md
