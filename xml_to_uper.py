#! /usr/bin/env python

# ****************************************************************************
# Author(s):
#   Ryan Ewing (January 2018)
#
# Copyright (C) 2017 -  OnBoard Security, Inc.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ****************************************************************************

'''
Description:
    A function to create an XML formatted BSM and convert it to UPER using the
    compiled J2735 ASN file. The ASN file was compiled into C using asn1c, and a
    C program called bsm_uper written and compiled to take in an XML formatted message
    and convert it to UPER using the ASN compiled C libraries.

    The function takes in the parameter you want to edit and a value, with defaults
    in case the value doesn't matter. Use as follows:

        uper_bsm = createXML_BSM(speed = '123', width='150')

    You can then send the BSM using the WSMP class send function.

    NOTE: Only creates unsigned BSMs
'''

import xml.etree.ElementTree as et
from xml.dom import minidom

# Function for using the ASN1C compiled DSRC files
# Creates an XML formatted BSM file which the C program reads and converts to UPER
# Requires a C binary called bsm_uper which takes in a BSM XML input file and a BSM UPER output file
def createXML_BSM(self, cnt='0', tid='41 42 43 44', sec=-1, lat='0', lon='0', elev='0', smaj='0', smin='0', orient='0',
                        transmission='neutral', speed='0', heading='0', angle='0', acc_long='0', acc_lat='0', acc_vert='0',
                        acc_yaw='0', wheelbrakes='1000', trac='unavailable', antibs='unavailable', stabcs='unavailable',
                        brakeb='unavailable', auxBrakes='unavailable', width='0', length='0'):
    if sec == -1:
        t = datetime.now()
        sec = str(t.second*1000 + t.microsecond/1000)


    # Use ElementTree to set up XML structure
    root = et.Element("MessageFrame")
    et.SubElement(root, "messageId").text = "20"
    value = et.SubElement(root, "value")
    bsm = et.SubElement(value, "BasicSafetyMessage")
    bsmcore = et.SubElement(bsm, "coreData")
    et.SubElement(bsmcore, "msgCnt").text = cnt
    et.SubElement(bsmcore, "id").text = tid
    et.SubElement(bsmcore, "secMark").text = sec
    et.SubElement(bsmcore, "lat").text = lat
    et.SubElement(bsmcore, "long").text = lon
    et.SubElement(bsmcore, "elev").text = elev

    acc = et.SubElement(bsmcore, "accuracy")
    et.SubElement(acc, "semiMajor").text = smaj
    et.SubElement(acc, "semiMinor").text = smin
    et.SubElement(acc, "orientation").text = orient

    tm = et.SubElement(bsmcore, "transmission")
    et.SubElement(tm, transmission)

    et.SubElement(bsmcore, "speed").text = speed
    et.SubElement(bsmcore, "heading").text = heading
    et.SubElement(bsmcore, "angle").text = angle

    accset = et.SubElement(bsmcore, "accelSet")
    et.SubElement(accset, "long").text = acc_long
    et.SubElement(accset, "lat").text = acc_lat
    et.SubElement(accset, "vert").text = acc_vert
    et.SubElement(accset, "yaw").text = acc_yaw

    brakes = et.SubElement(bsmcore, "brakes")
    et.SubElement(brakes, "wheelBrakes").text = wheelbrakes
    traction = et.SubElement(brakes, "traction")
    et.SubElement(traction, trac)

    ABS = et.SubElement(brakes, "abs")
    et.SubElement(ABS, antibs)

    scs = et.SubElement(brakes, "scs")
    et.SubElement(scs, stabcs)

    brakeBoost = et.SubElement(brakes, "brakeBoost")
    et.SubElement(brakeBoost, brakeb)

    aux = et.SubElement(brakes, "auxBrakes")
    et.SubElement(aux, auxBrakes)


    size = et.SubElement(bsmcore, "size")
    et.SubElement(size, "width").text = width
    et.SubElement(size, "length").text = length

    # Convert ElementTree to XML string
    xmlstr = "".join(et.tostringlist(root, encoding='utf8', method='xml')[1:])
    # Pretty print to console
    print minidom.parseString(et.tostring(root)).toprettyxml(indent="   ")

    # Write XML string to file
    with open("xml.dat", 'w+') as f:
        f.write(xmlstr)

    # Call the C binary that converts from XML to UPER
    call(["./bsm_uper xml.dat bsm.dat"], shell=True)

    # Read the UPER encoded file created by the C binary
    with open("bsm.dat", 'r') as f:
        bsm = f.read()

    return bsm
