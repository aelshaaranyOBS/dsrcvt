#! /usr/bin/env python

# ****************************************************************************
# Author(s):
#   Ahmed Elsharaany (September 2017)
#   Raashid Ansari (November 2017)
# Description:
#   A command-line program to create and send BSMs. Supports sending signed and
#   unsigned BSMs, with the option to edit values of message type, rate of
#   transmission and number of packets
#
# Copyright (C) 2017 - 2018 OnBoard Security, Inc.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ****************************************************************************

from datetime import datetime
# from ecdsa import SigningKey, NIST256p, util
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import utils

import time
import binascii
import os
import socket
import hashlib
import argparse

# Define argument parser
parser = argparse.ArgumentParser(description="Transmit [Un]Signed DSRC messages",
                                 version="1.0.1")

# Define optional arguments
parser.add_argument("-s", "--signed", action="store_true",
                    help="use to send signed messages, unsigned messages are sent otherwise")
parser.add_argument("-V", "--verbose", action="store_true",
                   help="show verbose logs as the program executes")
parser.add_argument("-i", "--udpip", default="127.0.0.1",
                    help="IP address of the host")
parser.add_argument("-p", "--udpport", default=52001, type=int,
                    help="port number in gnuradio_companion")

# Define positional arguments
parser.add_argument("msgtype", choices=['bsm', 'spat', 'map'],
                    help="type of message to be transmitted")
parser.add_argument("period", type=float, default=0,
                    help="rate of transmitting messages in seconds (0 for warp speed)")
parser.add_argument("num_packets", type=int, default=-1,
                    help="number of packets to transmit (-1 for infinite)")

# Parsing all arguments
args = parser.parse_args()

# Define UDP parameters
UDP_IP = args.udpip #localhost
UDP_PORT = args.udpport #port number in gnuradio_companion

# Define Constant 1970-2004 time difference
TIME_DIFF_CONST = 1072915200

# Define constant seconds offset
SECONDS_OFFSET = 0

# Define directory with utility files
UTILS_DIR = "./utils/"

## Define a function that transforms int to byte array
def intToByteArray(intValue, numBytes):
    # define empty byte array
    result = []

    # length of array
    intBytes = numBytes

    # define mask
    mask = 0xFF

    # fill the result array with the bytes
    for i in range(0, intBytes):
        result.insert(0, intValue & mask)
        intValue >>= 8

    # convert into chars
    for i in range(len(result)):
        result[i] = chr(result[i])

    return result

## Define a function that grabs byte array from binary file
## with a defined range of bytes, first byte is byte 0 and
## last byte index is included in the array
def getByteArray(fileHandle, startByte, stopByte):
    # define empty byte array
    byteArray = []

    # go to the first byte that needs to be read
    for i in range(startByte):
        fileHandle.read(1)

    # read the desired bytes one by one
    for i in range(startByte, stopByte+1):
        byteArray.append(fileHandle.read(1))

    # reset file cursor
    fileHandle.seek(0)

    return byteArray

## Define a function to generate time now bytes as per the
## 1609.2 requirement (might need to be modified with the
## number of leap seconds if OBU doesn't respond)
def getTimeBytes():
    # get current time stamp
    time_now = time.time() - SECONDS_OFFSET

    # subtract the difference between 1970 and 2004
    time_now_sub = time_now - TIME_DIFF_CONST

    # multiply by 1M and convert to int
    time_now_64 = int(time_now_sub * 1000000)

    # get the 8 bytes for the int value of time_now_64
    time_bytes = intToByteArray(time_now_64, 8)

    return time_bytes

## Define a function to define validity of Cert
def getCertValidityBytes():

    # get today's date
    day = datetime.today().day
    month = datetime.today().month
    year = datetime.today().year

    # get today's date
    time_now = (datetime(year,month,day,0,0) - datetime(1970,1,1)).total_seconds()

    # subtract the difference between 1970 and 2004
    time_now_sub = time_now - TIME_DIFF_CONST

    # multiply by 1M and convert to int
    time_now_64 = int(time_now_sub * 1000000)

    # get the 8 bytes for the int value of time_now_64
    time_bytes = intToByteArray(time_now_64, 4)

    return time_bytes


## Define craft BSM with valid signature function
def craftBSMValidSignature(bytes_before_time, time_bytes,  bytes_after_time, cert_hash_bytes, sign_key_bytes, bytes_after_time_before_signature):
    # to be signed data starts at byte 26 of the original BSM (i.e. after LLC and 1609.3 with a few bytes)
    to_be_signed_data = bytes_before_time[26:] + time_bytes

    # hash the to be signed data 
    to_be_signed_hash = hashlib.sha256(bytearray(to_be_signed_data))

    # concatenate the two hashes together and prepare it for signing
    concatenated_hash = to_be_signed_hash.hexdigest() + binascii.hexlify(''.join(cert_hash_bytes))

    # generate signing key from signing key bytes, and specifiy the eliptical curve
    # sk = SigningKey.from_string(bytearray(sign_key_bytes), curve=NIST256p)
    sk = ec.derive_private_key(int(str(bytearray(sign_key_bytes)).encode('hex'), 16), ec.SECP256R1(), default_backend())

    # set r and s lengths to 0 (sometimes signing generates r and s greater or less than 64)
    r_len = 0
    s_len = 0

    # loop until valid signature length is achived
    while( r_len != 64 or s_len != 64 ):
        # sign the concatenated hash (note that the signing function does hashing internally using the specificed hashing algorithm
        # so there is no need to do hashing before signing)
        # signature = sk.sign(bytearray.fromhex(concatenated_hash), hashfunc=hashlib.sha256)
        signature = sk.sign(str(bytearray.fromhex(concatenated_hash)), ec.ECDSA(hashes.SHA256()))

        # generate r and s from the signautre
        # [r,s] = util.sigdecode_string(signature, NIST256p.order)
        (r,s) = utils.decode_dss_signature(signature)

        # convert r and s to hex strings
        r_hex = hex(r)
        s_hex = hex(s)
        r_len = len(r_hex[2:len(r_hex)-1])
        s_len = len(s_hex[2:len(s_hex)-1])


    # generate the signed message through concatenation of bytes before time, time_bytes, bytes_after_time_before_signature, r, and s
    signed_message = binascii.hexlify(''.join(bytes_before_time[23:] + time_bytes + bytes_after_time_before_signature)) + r_hex[2:len(r_hex)-1] + s_hex[2:len(s_hex)-1]
    ''' For testing using Aerolink
    f_out = open("Attool_Signed_Message.dat","wb")
    f_out.write(bytearray.fromhex((signed_message)))
    f_out.close()
    '''
    # Concatenate signed message with LLC layer and 1609.3
    BSM = bytearray.fromhex(binascii.hexlify(''.join(bytes_before_time[0:23])) + signed_message)


    return BSM

## Define send BSM function
def sendSignedBSM(num_packets, bsm_rate, bytes_before_time, bytes_after_time, cert_hash_bytes, sign_key_bytes, bytes_after_time_before_signature, sock):

    # generate real time validity bytes
    valid_start_bytes = getCertValidityBytes()

    if(num_packets == -1):
        # define counter
        count = 1
        while True:
            # generate current time to add to BSM
            time_bytes = getTimeBytes()

            # craft BSM with valid signature and time
            BSM = craftBSMValidSignature(bytes_before_time, time_bytes,  bytes_after_time, cert_hash_bytes, sign_key_bytes, bytes_after_time_before_signature)

            # send the packet
            sock.sendto(BSM, (UDP_IP, UDP_PORT))

            # print packet number
            print("Real-Time Signed packet #"+str(count)+" sent")
            count +=1

            # sleep for bsm_rate
            time.sleep(bsm_rate)
    else:
        for i in range(num_packets):
            # generate current time to add to BSM
            time_bytes = getTimeBytes()

            # craft BSM with valid signature and time
            BSM = craftBSMValidSignature(bytes_before_time, time_bytes,  bytes_after_time, cert_hash_bytes, sign_key_bytes, bytes_after_time_before_signature)

            # send the packet
            sock.sendto(BSM, (UDP_IP, UDP_PORT))

            # print packet number
            print("Real-Time Signed packet #"+str(i+1)+" sent")

            # sleep for bsm_rate
            time.sleep(bsm_rate)

# Define send Unsigned BSM function
def sendUnsignedBSM(num_packets, bsm_rate, unsigned_BSM, sock):
    if(num_packets == -1):
        # define counter
        count = 1
        while True:
            # send the packet
            sock.sendto(unsigned_BSM, (UDP_IP, UDP_PORT))

            # print packet number
            print("Unsigned packet #"+str(count)+" sent")
            count +=1

            # sleep for bsm_rate
            time.sleep(bsm_rate)
    else:
        for i in range(num_packets):
            # send the packet
            sock.sendto(unsigned_BSM, (UDP_IP, UDP_PORT))

            # print packet number
            print("Unsigned packet #"+ str(i+1) +" sent")

            # sleep for bsm_rate
            time.sleep(bsm_rate)

# Define main function
def main():

    # Print program start display
    print("********************************************************")
    print("*                     Attacker Tool                    *")
    print("********************************************************")

    # Print BSM options
    # Read BSM option
    signed_bsm = args.signed

    # Print rate inquiry
    # Read rate
    bsm_rate = args.period

    # Print number of packets inquiry
    # Read num_packets
    num_packets = args.num_packets

    print("Setting up UDP socket")
    time.sleep(0.5)
    # set up the UDP_socket
    sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP

    # if real-time signed BSM, get bytes before and after time
    # generation bytes
    if(signed_bsm):
        # define file name and path
        cert_file_name = "BSM_certificate.dat"
        cert_file_path = UTILS_DIR + cert_file_name

        # open file to read binary data
        f_in = open(cert_file_path, "rb")

        # get total number of bytes in file
        file_num_bytes = os.path.getsize(cert_file_path)

        # get bytes before and after time bytes
        bytes_before_time = getByteArray(f_in, 0, 96)
        #bytes_after_time_before_valid  = getByteArray(f_in, 105, 151)
        bytes_after_time = getByteArray(f_in, 105, file_num_bytes-1)
        bytes_after_time_before_signature = getByteArray(f_in, 105, 223)

        # create cert hash file name and path
        cert_hash_file_name = "Cert_Hash.dat"
        cert_hash_file_path = UTILS_DIR + cert_hash_file_name

        # open file to read and size
        cert_f_in = open(cert_hash_file_path, "rb")
        cert_hash_file_size = os.path.getsize(cert_hash_file_path)

        # get certificate hash bytes
        cert_hash_bytes = getByteArray(cert_f_in, 0, cert_hash_file_size-1)

        # create signing key file name and path
        sign_key_file_name = "Sign_Key.dat"
        sign_key_file_path = UTILS_DIR + sign_key_file_name

        # open file to read and size
        sk_f_in = open(sign_key_file_path, "rb")
        sign_key_file_size = os.path.getsize(sign_key_file_path)

        # get signing key bytes
        sign_key_bytes = getByteArray(sk_f_in, 0, sign_key_file_size-1)

        # Send signed BSMs
        sendSignedBSM(num_packets, bsm_rate, bytes_before_time, bytes_after_time, cert_hash_bytes, sign_key_bytes, bytes_after_time_before_signature, sock)

    # if Unsigned BSM
    else:
        # define file name
        bsm_file_name="unsigned_BSM.dat" # This file should contain the unsigned BSM sent earlier by your OBU
        bsm_file_path = UTILS_DIR + bsm_file_name

        # read file content
        with open(bsm_file_path, mode='rb') as file: # b is important -> binary
            unsigned_BSM = file.read()

        # send unsigned BSMs
        sendUnsignedBSM(num_packets, bsm_rate, unsigned_BSM, sock)

    # close UDP socket
    sock.close()

    print
    print(str(num_packets)+" Packets Sent")
    print("Program Terminated!")

if __name__ == "__main__":
    main()

