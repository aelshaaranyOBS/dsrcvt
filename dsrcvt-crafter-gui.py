#! /usr/bin/env python

# ****************************************************************************
# Author(s):
#   Ryan Ewing (January 2018)
# Description:
#   A GUI version of the dsrcvt-crafter.py program to allow easier
#   modification and sending of a BSM. Supports manual editing of parameters
#   and loading of a JSON file with the parameter and values that a user 
#   wants changed.
#
# Copyright (C) 2017 - 2018 OnBoard Security, Inc.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ****************************************************************************

import tkinter as tk
from tkinter import messagebox
import ttk
import os
import json

from tkinter import filedialog
from utils.wsmp import WSMP

class UI(tk.Frame):
    # Initialize BSM packet class and UI
    def __init__(self, master):
        self.bsm = WSMP(False)

        tk.Frame.__init__(self, master)
        self.master = master
        self.initUI()

    # More initializations, create title, buttons, text fields etc
    def initUI(self):
        self.master.title("DSRC Attacker Tool")
        self.pack(fill=tk.BOTH, expand=True)
        self.centerWindow()
        self.configure(bg="white")

        frame_title = tk.Frame(self, bg = "white")
        frame_title.pack(fill=tk.X)
        lbl_title = tk.Label(frame_title, text = "DSRC Attacker Tool", font=(None, 25, "bold"), width = 25,bg = "white", fg="#000033")
        lbl_title.pack(padx=20, pady=10)

        # add the top menubar
        menubar = tk.Menu(self, background="#EEEEEE", activebackground="#EEEEEE")
        self.master.config(menu=menubar)
        fileMenu = tk.Menu(menubar, background="#EEEEEE")
        fileMenu.add_command(label="Open", command=self.openFile)
        fileMenu.add_command(label="Exit", command=self.onExit)
        menubar.add_cascade(label="File", menu=fileMenu)

        # add a left column to put content in
        frame_left = tk.Frame(self, bg="white")
        frame_left.pack(fill=tk.BOTH, side=tk.LEFT, expand=True)

        self.cur_param = tk.StringVar()
        # self.cur_param.set('bsm_id') # set if you want a default value to start with
        self.drop = tk.OptionMenu(frame_left, self.cur_param,*self.bsm.getParameters().keys(),command=self.updateBSMDescription)
        self.drop.configure(height = 2, width=40, bg="#EEEEEE")
        self.drop.pack(side=tk.TOP, anchor=tk.N, padx=15, pady=25)

        self.text_info = tk.Text(frame_left, height= 15, width = 50)
        self.text_info.configure(bg="#EEEEEE")
        self.text_info.pack(side = tk.TOP, anchor=tk.N, padx=15)


        # add a middle frame to put content in
        frame_mid = tk.Frame(self, bg="white")
        frame_mid.pack(fill = tk.BOTH, side=tk.LEFT, expand = True)

        text_input = tk.Entry(frame_mid, width = 30)
        text_input.configure(bg="#EEEEEE")
        text_input.pack(side=tk.TOP, anchor=tk.N, padx=15, pady=32, ipady=5)
        text_input.insert(tk.END, "Enter Value")
        text_input.bind("<Button-1>", self.clearEntryInfo)
        text_input.bind("<Return>", self.getEntryInfo)


        # add a right frame to put content in
        frame_right = tk.Frame(self, bg="white")
        frame_right.pack(fill = tk.BOTH, side=tk.LEFT, expand = True)

        self.text_bsm = tk.Text(frame_right, height = 100)
        self.text_bsm.configure(bg="#EEEEEE")
        self.text_bsm.pack(side=tk.TOP, anchor=tk.N, padx=15, pady=25, expand=True)
        self.text_bsm.config(state=tk.DISABLED)

        # IP field
        self.entry_ip = tk.Entry(frame_mid, text="UDP_IP")
        self.entry_ip.pack(anchor=tk.S)
        self.entry_ip.insert(tk.END, "localhost")
        self.entry_ip.bind("<Button-1>", self.clearEntryInfo)

        # Port field
        self.entry_port = tk.Entry(frame_mid, text="UDP_PORT")
        self.entry_port.pack(anchor=tk.S)
        self.entry_port.insert(tk.END, "52001")
        self.entry_port.bind("<Button-1>", self.clearEntryInfo)

        # Send packet button
        button_send = tk.Button(frame_mid, text="Send Packet", command=self.send_packet)
        button_send.pack(anchor=tk.S)

        # Checkbutton to send signed BSMs
        self.is_signed_packet = tk.IntVar()
        signed = tk.Checkbutton(frame_mid, text="Signed", variable=self.is_signed_packet)
        signed.pack(anchor=tk.S)

        # update BSM text and descriptions once to populate at initial startup
        self.updateBSMText()
        self.updateBSMDescription(self.cur_param.get())

    # Updates the BSM parameters and values text
    def updateBSMText(self):
        self.clearText(self.text_bsm)
        for p, v in self.bsm.getParameters().iteritems():
            self.writeText(self.text_bsm, "%s\t\t\t%s" %(p, v[0]))

    # Shows the description for the current parameter
    def updateBSMDescription(self, value):
        self.clearText(self.text_info)
        if value != '':
            self.writeText(self.text_info, self.bsm.getParameters()[value][3])

    # Quit program gracefully
    def onExit(self):
        self.quit()

    # Opens JSON file, shows a file dialog box
    # Ensure JSON file is formatted as follows:
    '''
        {
            "speed": 50,
            "elevation": 1234
        }

    '''
    # Where each line except for the last ends in a comma and everything is 
    # surrounded by curly brackets
    def openFile(self):
        ftypes = [('JSON files', '.json'), ('all files', '.*')]
        file = filedialog.askopenfilename(initialdir=os.getcwd(), 
                title = "Select a JSON file", filetypes = ftypes)

        if file:
            try:
                with open(file, 'r') as f:
                    data = f.read()
                    new_params = json.loads(data)
                for p in new_params:
                    self.bsm.edit(p, new_params[p])
                self.updateBSMText()

            except Exception as e:
                print e
                tk.messagebox.showerror("ERROR", "Could not open JSON file.")

    # Calculates center and places windows there
    def centerWindow(self):
        width = 1280
        height = 720
        screen_w = self.master.winfo_screenwidth()
        screen_h = self.master.winfo_screenheight()
        x = (screen_w - width) / 2
        y = (screen_h - height) / 2
        self.master.geometry('%dx%d+%d+%d' %(width, height, x, y))

    # Write text to a text field that normally has write disabled
    def writeText(self, widget, text):
        widget.config(state=tk.NORMAL)
        widget.insert(tk.END, "%s\n" %(text))

        widget.config(state=tk.DISABLED)

    # Clear text in a text field that normally has write disabled
    def clearText(self, widget):
        widget.config(state=tk.NORMAL)
        widget.delete("1.0", tk.END)
        widget.config(state=tk.DISABLED)

    # Clear text in an entry field
    def clearEntryInfo(self, event):
        event.widget.delete(0, tk.END)

    # Get current entered text in an entry field
    def getEntryInfo(self, event):
        self.bsm.edit(self.cur_param.get(), event.widget.get())
        self.updateBSMText()
        self.clearEntryInfo(event)

    # Send WSM packet
    def send_packet(self):
        ip = self.entry_ip.get()
        port = self.entry_port.get()
        try:
            wsm = self.bsm.construct(self.is_signed_packet.get())
            self.bsm.send(wsm, ip, port)

            print "Successfully sent BSM to %s:%s" %(ip, port)
        except Exception as e:
            print e
            tk.messagebox.showerror("ERROR", "Could not send packet to %s:%s" %(ip, port))

def main():
    root = tk.Tk()
    app=UI(root)
    root.mainloop()


if __name__ == '__main__':
    main()
